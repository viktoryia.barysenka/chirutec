<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package chiruTec
 */

?>

<footer id="colophon" class="site-footer">
    <div class="container">
        <div class="site-info">
            <div class="flex-footer-copyright">
                <div class="copyright">
                    © 2021 ChiruTec GmbH. All Rights Reserved.
                </div>
                <div class="flex-footer">
                    <div class="item">
                        <a href=""> Allgemeine Geschäftsbedingungen</a>
                    </div>
                    <div class="item">
                        <a href="">
                            Datenschutzerklärung
                        </a>
                    </div>
                    <div class="item">
                        <a href="">
                            Impressum
                        </a>
                    </div>
                </div>
            </div>
        </div><!-- .site-info -->
    </div>
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
