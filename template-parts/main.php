<?php
/*
Template Name: main-template
*/

get_header();
?>
    <div class="intro">
        <div class="container">
            <div class="background-image">
            </div>
            <div class="gray-line">
            </div>
            <div class="blue-line">
            </div>
            <div class="introduction">
                <div class="title">
                    Ihr Partner für den
                    medizinischen Bedarf
                </div>
                <div class="text">
                    Konzentration auf das Kerngeschäft / Erhöhung der Wertschöpfung
                </div>
                <button type="button" class="btn btn-main "> Gleich loslegen?</button>
            </div>
        </div>
    </div>

    <div class="advantages">
        <div class="container">
            <div class="advantages-items">
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <img src="<?php echo get_template_directory_uri() . "/assets/images/activity.svg"; ?>" alt="">
                    <div class="title">
                        Einkauf von Medizinprodukten
                    </div>
                    <div class="description">
                        Wir sparen für Sie Sachkosten ein
                    </div>
                    <a href="">
                        Erfahren Sie mehr
                    </a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <img src="<?php echo get_template_directory_uri() . "/assets/images/map.svg"; ?>" alt="">
                    <div class="title">
                        Strategisches Einkaufs Logistikmanagement
                    </div>
                    <div class="description">
                        Wir entwickeln für Sie eine nachhaltige Einkaufsstrategie
                    </div>
                    <a href="">
                        Erfahren Sie mehr
                    </a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <img src="<?php echo get_template_directory_uri() . "/assets/images/globe.svg"; ?>" alt="">
                    <div class="title">
                        Direktbezug aus Asien
                    </div>
                    <div class="description">
                        Wir beziehen für Sie direkt vom Hersteller
                    </div>
                    <a href="">
                        Erfahren Sie mehr
                    </a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <img src="<?php echo get_template_directory_uri() . "/assets/images/union.svg"; ?>" alt="">
                    <div class="title">
                        Smarte IT-Lösungen
                    </div>
                    <div class="description">
                        Wir vereinfachen für Sie Arbeitsabläufe
                    </div>
                    <a href="">
                        Erfahren Sie mehr
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="posts">
        <div class="container">
            <div class="title">
                Verlässliche und starke Partner
            </div>
            <div class="desc">
                Durch unsere sehr engen Partnerschaften mit namhaften Unternehmen, die alle Spezialisten in ihrem
                Bereich sind, können Sie sich vollumfänglich auf Ihr Kerngeschäft
                konzentrieren und die Wertschöpfung in Ihrem Hause erhöhen.
            </div>
            <div class="aa">
                <div class="col-xs-12 col-sm-6 col-md-4 posts-item">
                    <img src="<?php echo get_template_directory_uri() . "/assets/images/quotes.svg"; ?>" alt="">
                    <div class="post-desc">Für unsere Logistik in der Schweiz nutzen wir das hochmoderne Lager für
                        Medizinprodukte der
                        Post Schweiz in Villmergen. Es ist gemäß ISO 9001/14001
                        zertifiziert und besitzt die Swissmedic-Bescheinigung. Darüber hinaus ist das Lager in
                        VillmergenGMP- und GDP-zertifiziert*. Zu den Stärken der Post
                        gehört auch die gesetzeskonforme Lagerung von Arznei mitteln aller relevanten Gefahrstoffklassen
                        und das Chargen- und Verfallsdatenmanagement. Das
                        feinmaschige Transportnetz der Post ermöglicht eine schnelle, schweizweite Zustellung. Die
                        Anlieferungen können aufZeitfenster mit geringer Auslastung der spitalinternen Betriebswege
                        abgestimmt werden, mit Tagexpress und mit Swiss-Express „Innight“ auch nachts.
                    </div>
                    <img src="<?php echo get_template_directory_uri() . "/assets/images/die-post.svg"; ?>" alt="">
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 posts-item">
                    <img src="<?php echo get_template_directory_uri() . "/assets/images/quotes.svg"; ?>" alt="">
                    <div class="post-desc">Zusammen mit der Lufthansa Industry Solutions können wir Ihnen smarte
                        IT-Lösungen
                        im Gesundheits- und Spitalwesen anbieten. Mit über 20 Jahren branchenübergreifender IT-Kompetenz
                        der Lufthansa-Tochterfirma und mit interdisziplinären Teams an 14 Standorten weltweit (davon
                        zwei in der
                        Schweiz) deckt die Lufthansa Industry Solutions das komplette Spektrum an IT-Dienstleistungen
                        ab. Im Jahr 2020 gehörte sie zu den besten IT-Dienstleis tern in Deutschland.
                    </div>
                    <img src="<?php echo get_template_directory_uri() . "/assets/images/lufthansa.svg"; ?>" alt="">
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 posts-item">
                    <img src="<?php echo get_template_directory_uri() . "/assets/images/quotes.svg"; ?>" alt="">
                    <div class="post-desc">Die Life Stage Solutions ist unser Partner für den Markt der Altersmedizin in
                        der
                        Schweiz, spezialisiert in der Digitalisierung des ambulanten Pflegemarktes.
                        Neben dem klassischen Handel mit Medizinprodukten für den Pflegemarkt erarbeitet die Life Stage
                        Solutions gemeinsam mit ihren Kunden Lösungen,
                        welche die Arbeitsabläufe vereinfachen.
                    </div>
                    <img src="<?php echo get_template_directory_uri() . "/assets/images/lifestage.svg"; ?>" alt="">
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 posts-item">
                    <img src="<?php echo get_template_directory_uri() . "/assets/images/quotes.svg"; ?>" alt="">
                    <div class="post-desc">DNA4GOOD bietet individuelle, molekulare Analysen in Verbindung mit
                        ärztlicher
                        Beratung an. Zielsetzung ist, mit Hilfe von hochwertigen Tests eine
                        effektive, personalisierte
                        Gesundheitsvorsorge im Kontext der betrieblichen Prozesse zu ermöglichen. Seit Ausbruch der
                        COVID-19 Pandemie entwickelt DNA4GOOD gemeinsam
                        mit Unternehmen Pandemiepläne und ein digitales Testmanagementsystem für Covid-19
                        Schnelltestungen mit dem Ziel einen Gesundheitsschutz in der
                        „neuen Normalität“ zu entwickeln.
                    </div>
                    <img src="<?php echo get_template_directory_uri() . "/assets/images/dna.svg"; ?>" alt="">
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 posts-item">
                    <img src="<?php echo get_template_directory_uri() . "/assets/images/quotes.svg"; ?>" alt="">
                    <div class="post-desc">Die miralytik healthcare consulting berät Spitäler im Bereich des Medizin-
                        und
                        Sachkostencontrollings. Hierfür hat sie eine Software entwickelt welches den Einkauf und das
                        Medizincontrolling miteinander verknüpft und zwar auf Basis der DRGs. Die miralytik besitzt
                        langjährige Spitalerfahrung und umfang-reiche Erfahrung aus anderen Wirtschaftszweigen. Ihre
                        Softwarelösungen werden ständig auf Basis aktueller Herausforderungen im Gesundheitswesen
                        weiterentwickelt.
                    </div>
                    <img src="<?php echo get_template_directory_uri() . "/assets/images/mira.svg"; ?>" alt="">
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 posts-item">
                    <img src="<?php echo get_template_directory_uri() . "/assets/images/quotes.svg"; ?>" alt="">
                    <div class="post-desc">Die aifinyo ist ein börsennotierter Finanzdienstleister und bietet
                        aufeinander
                        abgestimmte Liquiditätslösungen u.a. für die Waren- und Projektfinanzierung aus einer Hand.
                        Dabei ist das oberste Ziel der aifinyo hierfür smarte und digitale Lösungen bereitzustellen.
                    </div>
                    <img src="<?php echo get_template_directory_uri() . "/assets/images/aifinyo.svg"; ?>" alt="">
                </div>
            </div>
        </div>
    </div>

    <div class="digit-block">
        <div class="container">
            <div class="digits">
                <div class="col-xs-12 col-sm-6 col-md-4 posts-item">
                    <div class="digit">500 +</div>
                    <div class="text">Anzahl Kunden</div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 posts-item">
                    <div class="digit">100 +</div>
                    <div class="text">Anzahl Lieferanten</div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 posts-item">
                    <div class="digit">2.500 +</div>
                    <div class="text">Anzahl Lagerartikel</div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 posts-item">
                    <div class="digit">2</div>
                    <div class="text">Anzahl Lager</div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 posts-item">
                    <div class="digit">97% +</div>
                    <div class="text">Lieferquote</div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 posts-item">
                    <div class="digit">25% +</div>
                    <div class="text">Durchschnittliche Preisersparnis</div>
                </div>
            </div>
        </div>
    </div>

    <div class="focus">
        <div class="container">
            <div class="title">
                Fokussierung auf das Wesentliche
            </div>
            <div class="focus-items">
                <div class="col-xs-12 col-md-6">
                    <img src="<?php echo get_template_directory_uri() . "/assets/images/focus-image.png"; ?>"
                         class="focus-image" alt="">
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="flex-blocks">
                        <div class="block">
                            <img src="<?php echo get_template_directory_uri() . "/assets/images/arrow.svg"; ?>" alt="">
                        </div>
                        <div class="block">
                            <div class="text">
                                Passende Einkaufsstrategie
                                <p>
                                    ChiruTec entwickelt mit Ihnen eine nachhaltige Einkaufsstrategie.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="flex-blocks">
                        <div class="block">
                            <img src="<?php echo get_template_directory_uri() . "/assets/images/arrow.svg"; ?>" alt="">
                        </div>
                        <div class="block">
                            <div class="text">
                                Schnelle Lieferung
                                <p>
                                    Lieferzeit 1–2 Tage innerhalb der Schweiz, bei Bedarf sind auch
                                    Expresslieferungen am selben Tag möglich.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="flex-blocks">
                        <div class="block">
                            <img src="<?php echo get_template_directory_uri() . "/assets/images/arrow.svg"; ?>" alt="">
                        </div>
                        <div class="block">
                            Hohe Qualität & Sicherheit
                            <p>
                                Dauerhaft günstige Preise bei sehr hoher Liefersicherheit (> 98 %).
                            </p>
                        </div>
                    </div>
                    <div class="flex-blocks">
                        <div class="block">
                            <img src="<?php echo get_template_directory_uri() . "/assets/images/arrow.svg"; ?>" alt="">
                        </div>
                        <div class="block">
                            Ganzheitliche Lösungskonzepte
                            <p>
                                Wir bieten Ihnen Lösungen rund um das Produkt an.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
<?php
get_footer();
?>