<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package chiruTec
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() . "/assets/scss/header.css"; ?>" type="text/css"/>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() . "/assets/scss/footer.css"; ?>" type="text/css"/>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() . "/assets/scss/main-template.css"; ?>" type="text/css"/>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="header" class="site">
    <div class="container">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'chirutec' ); ?></a>

	<header id="masthead" class="site-header">

		<nav id="site-navigation" class="main-navigation">
            <div class="flex">
                <div class="site-branding logo-desktop">
                    <?php
                    the_custom_logo();
                    if ( is_front_page() && is_home() ) :
                        ?>
                        <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
                    <?php
                    else :
                        ?>
                        <p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
                    <?php
                    endif;
                    $chirutec_description = get_bloginfo( 'description', 'display' );
                    if ( $chirutec_description || is_customize_preview() ) :
                        ?>
                        <p class="site-description"><?php echo $chirutec_description; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
                    <?php endif; ?>
                </div><!-- .site-branding -->
                <?php
                wp_nav_menu(
                    array(
                        'theme_location' => 'menu-1',
                        'menu_id'        => 'primary-menu',
                    )
                );
                ?>
                <div class="site-branding logo-mobile">
		            <?php
		            the_custom_logo();
		            if ( is_front_page() && is_home() ) :
			            ?>
                        <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
		            <?php
		            else :
			            ?>
                        <p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
		            <?php
		            endif;
		            $chirutec_description = get_bloginfo( 'description', 'display' );
		            if ( $chirutec_description || is_customize_preview() ) :
			            ?>
                        <p class="site-description"><?php echo $chirutec_description; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
		            <?php endif; ?>
                </div><!-- .site-branding -->
            </div>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->
    </div>
